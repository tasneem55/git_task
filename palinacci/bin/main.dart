import 'dart:io';

import 'package:palinacci/palindrome.dart' as palindrome;

import 'package:palinacci/fibonacci.dart' as fib;

void main(List<String> arguments) {
  //if you're using VS code, please run "main" from terminal using "dart run bin\main.dart",
  //apparently VS code does not support stdin
  print('Enter a word');
  String? word = stdin.readLineSync();
  palindrome.isPalindrome(word);
  int? n;
  print('Enter nth fibonacci number');
  try {
    n = int.parse(stdin.readLineSync() ?? '0');
    print(fib.fibonacci(n));
  } catch (e) {
    print("invalid number");
  }
}
