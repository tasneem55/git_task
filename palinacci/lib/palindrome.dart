void isPalindrome(String? word) {
  //"split" will return a list of the characters of the word entered
  //then "reverse" will reverse this list
  //and finally "join" will concatinate this reversed list back together as a single string
  String? reverse = word!.split('').reversed.join('');

  if (word == reverse) {
    print('$word is a Palindrome');
  } else {
    print('$word is not a Palindrome');
  }
}
